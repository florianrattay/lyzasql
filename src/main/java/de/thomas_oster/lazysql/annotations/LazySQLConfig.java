/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface LazySQLConfig {
    /**
     * Path to a JSON file with config parameters
     * @return 
     */
    String file() default "";
    /**
     * URL for JDBC Connection (only used during compilation)
     * e.g. jdbc:sqlserver://localhost;databaseName=whateveryoulike
     * @return 
     */
    String dburl() default "";
    /**
     * Username for the database connection
     * @return 
     */
    String user() default "";
    /**
     * password for the database connection
     * @return 
     */
    String password() default "";
    /**
     * wether this configuration is only for the annotated class (false)
     * or for the whole project. Annotations on the class override
     * global ones.
     * @return 
     */
    boolean global() default true;
    /**
     * If set, all DB Queris will be cached to that file
     * @return 
     */
    String cacheFilePath() default "";
    /**
     * If set, we try to register this driver before connecting to the db.Not necessary for all supported JDBC drivers
     * @return
     */
    String customDatabaseDriver() default "";
    /**
     * If set, we use @Inject for injecting the datasource in the generated
     * classes
     * @return 
     */
    boolean dependencyInjection() default false;
    /**
     * If set, the getters generated make the first letter uppercase
     * if not, they are created
     * @return if getters/setters should be capitalized
     */
    boolean addJsonAnnotations() default false;

    /**
     * The Columns with names in here are
     * excluded from the result type.
     * Useful for select* queries where you
     * want to exclude certain columns.
     * @return
     */
    String[] excludeColumns() default "";
}
