/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import de.thomas_oster.lazysql.AbstractRDBMSAdapter.Procedure;
import de.thomas_oster.lazysql.Lazy.Supplier;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class ArgumentListCache {

    private final File cachefile;
    private static class ArgumentListOrException {
        SQLException exception = null;
        List<Argument> list = null;
        List<String> strings = null;
        List<Procedure> procedures = null;
    }
    
    private Map<String, ArgumentListOrException> data = new LinkedHashMap<>();

    public ArgumentListCache(File cachefile) {
        this.cachefile = cachefile;
        if (cachefile != null) {
            if (LazySQLProcessor.DEBUG_TO_FILE) {
                LazySQLProcessor.debugLog("Using Cachefile "+cachefile.getAbsolutePath());
            }
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                saveToDisk();
            }));
            readFromDisk();
        }
    }

    public final void readFromDisk() {
        try (FileInputStream fis = new FileInputStream(cachefile);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            @SuppressWarnings("unchecked")
            Map<String, String> rdata = (Map<String, String>) ois.readObject();
            if (rdata != null) {
                for (Entry<String, String> e:rdata.entrySet()) {
                    ArgumentListOrException d = new ArgumentListOrException();
                    String s = e.getValue();
                    try {
                        if (s.startsWith("SQLEXCEPTION###")) {
                            String[] parts = s.split("###");
                            d.exception = new SQLException(parts[3], parts[2], Integer.parseInt(parts[1]));
                        }
                        else if (s.startsWith("STRINGLIST###")) {
                            d.strings = Arrays.asList(s.substring(13).split(","));
                        }
                        else if (s.startsWith("PROCEDURES###")) {
                            d.procedures = Arrays.stream(s.substring(13).split(",")).map((st) -> {
                                Procedure p = new Procedure();
                                String[] pa = st.split("##");
                                p.name = pa[0];
                                p.comment = "null".equals(pa[1]) ? null : pa[1];
                                return p;
                            }).collect(Collectors.toList());
                        }
                        else {
                            d.list = Argument.fromTypeListString((String) e.getValue());
                        }
                        data.put(e.getKey(), d);
                    }
                    catch (Exception ex) {
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public List<Procedure> getProcedures(String key, Supplier<List<Procedure>,Exception> calc) throws SQLException, AbstractRDBMSAdapter.NotSupportedException  {
        ArgumentListOrException d = new ArgumentListOrException();
        if (!data.containsKey(key)) {
            try {
                d.procedures = calc.get();
            }
            catch (SQLException e) {
                //SQL Exceptions are from the query and should be cached
                d.exception = e;
            }
            catch (AbstractRDBMSAdapter.NotSupportedException e) {
                //not supported should be thrown
                throw e;
            }
            catch (Exception e) {
                //should not happen
                throw new SQLException(e);
            }
            data.put(key, d);
        }
        d = data.get(key);
        if (d.exception != null) {
            throw d.exception;
        }
        return d.procedures;
    }
    
    public List<String> getStrings(String key, Supplier<List<String>,Exception> calc) throws SQLException, AbstractRDBMSAdapter.NotSupportedException  {
        ArgumentListOrException d = new ArgumentListOrException();
        if (!data.containsKey(key)) {
            try {
                d.strings = calc.get();
            }
            catch (SQLException e) {
                //SQL Exceptions are from the query and should be cached
                d.exception = e;
            }
            catch (AbstractRDBMSAdapter.NotSupportedException e) {
                //not supported should be thrown
                throw e;
            }
            catch (Exception e) {
                //should not happen
                throw new SQLException(e);
            }
            data.put(key, d);
        }
        d = data.get(key);
        if (d.exception != null) {
            throw d.exception;
        }
        return d.strings;
    }
    
    public List<Argument> get(String key, Supplier<List<Argument>, Exception> calc) throws SQLException, AbstractRDBMSAdapter.NotSupportedException {
        ArgumentListOrException d = new ArgumentListOrException();
        if (!data.containsKey(key)) {
            try {
                d.list = calc.get();
            }
            catch (SQLException e) {
                //SQL Exceptions are from the query and should be cached
                d.exception = e;
            }
            catch (AbstractRDBMSAdapter.NotSupportedException e) {
                //not supported should be thrown
                throw e;
            }
            catch (Exception e) {
                //should not happen
                throw new SQLException(e);
            }
            data.put(key, d);
        }
        else if (LazySQLProcessor.DEBUG_TO_FILE) {
            LazySQLProcessor.debugLog("Result from cache");
        }
        d = data.get(key);
        if (d.exception != null) {
            throw d.exception;
        }
        return d.list;
    }

    public void saveToDisk() {
        Map<String,String> wdata = new LinkedHashMap<>();
        for (Entry<String, ArgumentListOrException> e:data.entrySet()) {
            String s;
            if (e.getValue().exception != null) {
                SQLException ex = e.getValue().exception;
                s = "SQLEXCEPTION###"+ex.getErrorCode()+"###"+ex.getSQLState()+"###"+ex.getMessage();
            }
            else if (e.getValue().strings != null) {
                s = "STRINGLIST###"+e.getValue().strings.stream().collect(Collectors.joining(","));
            }
            else if (e.getValue().procedures != null) {
                s = "PROCEDURES###"+e.getValue().procedures.stream().map(
                    (p) -> p.name+"##"+p.comment
                ).collect(Collectors.joining(","));
            }
            else {
                s = Argument.toTypeListString(e.getValue().list);
            }
            wdata.put(e.getKey(), s);
        }
        try (FileOutputStream fos = new FileOutputStream(cachefile);
                ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(wdata);
        } catch (IOException ex) {
            Logger.getLogger(ArgumentListCache.class
                .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
