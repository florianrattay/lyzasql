/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.squareup.javapoet.TypeName;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.lang.model.element.VariableElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"name", "type", "dir"})
public class Argument {

    public static enum Direction {
        IN,
        OUT,
        INOUT
    }
    
    String name;
    TypeName type;
    Direction dir;
    boolean optional = false;

    public static class MismatchException extends ElementException {
        public MismatchException(String message) {
            super(message);
        }
    };
    
    public static class ParsingException extends ElementException {
        public ParsingException(String message, Exception cause) {
            super(message);
        }
    };

    public Argument(VariableElement v) {
        this(v.getSimpleName().toString(), TypeName.get(v.asType()), Direction.IN, false);
    }

    public Argument(String name, Type c, boolean optional) {
        this(name, TypeName.get(c), Direction.IN, optional);
    }
    
    public Argument(String name, TypeName c, boolean optional) {
        this(name, c, Direction.IN, optional);
    }

    public Argument(String name, Type c) {
        this(name, TypeName.get(c), Direction.IN, false);
    }

    public Argument(String name, TypeName c) {
        this(name, c, Direction.IN, false);
    }

    public static List<Argument> fromVariableElements(List<? extends VariableElement> l) {
        return l.stream().map(Argument::new).collect(Collectors.toList());
    }

    static String toArgumentList(List<Argument> params) {
        if (params == null) {
            return "";
        }
        return params.stream().map(Argument::toString).collect(Collectors.joining(", "));
    }

    static void compareWithOrder(List<Argument> a, List<Argument> b) throws MismatchException {
        Iterator<Argument> ia = a.iterator();
        Iterator<Argument> ib = b.iterator();
        while (ia.hasNext() && ib.hasNext()) {
            Argument bb = ia.next();
            Argument aa = ib.next();
            if (!bb.equals(aa)) {
                throw new MismatchException("Parameters " + aa.getName() + " and " + bb.getName() + " differ. Should be " + Argument.toArgumentList(b));
            }
        }
        if (ia.hasNext()) {
            throw new MismatchException("More parameters in the stored procedure than in the method.Should be " + Argument.toArgumentList(b));
        } else if (ib.hasNext()) {
            throw new MismatchException("More parameters in the method than in the stored procedure.Should be " + Argument.toArgumentList(b));
        }
    }
   
    public static void compareWithoutOrder(Collection<Argument> a, Collection<Argument> b) throws MismatchException {
        try {
            Map<String,Argument> mb = b.stream().collect(Collectors.toMap(Argument::getName, x -> x));
            for (Argument ia : a) {
                Argument bb = mb.get(ia.getName());
                if (bb == null) {
                    throw new MismatchException("Parameter "+ia+" is missing");
                }
                else if (!bb.getType().equals(ia.getType())) {
                    throw new MismatchException("Parameter "+ia+" has wrong type "+ia.getType()+" instead of "+bb.getType());
                }
                else if (!bb.getDir().equals(ia.getDir())) {
                    throw new MismatchException("Parameter "+ia+" has wrong direction "+ia.getDir()+" instead of "+bb.getDir());
                }
            }
            for (Argument ib : b) {
                if (!a.contains(ib)) {
                    throw new MismatchException("Parameter "+ib+" is too much");
                }
            }
        }
        catch (IllegalStateException e) {
            throw new MismatchException(e.getLocalizedMessage());
        }
    }

    public static String toTypeListString(Collection<Argument> type) {
        return type.stream()
            .map(Argument::toString)
            .collect(Collectors.joining(", "));
    }

    /**
     * Liest einen String wie zB "String variable1, Integer variable 2" usw ein
     * @param string the arguments as string like "String var1, Integer var2..."
     * @return a list of Arguments corresponding to the string
     * @throws de.thomas_oster.lazysql.Argument.ParsingException if there is an error 
     */
    public static List<Argument> fromTypeListString(String string) throws ParsingException {
        List<Argument> result = new LinkedList<>();
        try {
            if (string.isEmpty()) {
                return result;
            }
            for (String part: string.split(", ?")) {
                String[] parts = part.trim().split(" ", 3);
                if (parts.length == 2) {
                    result.add(new Argument(parts[1].trim(), TypeHelper.typeFromString(parts[0].trim()), false));
                }
                else if (parts.length == 3) {
                    result.add(new Argument(parts[2].trim(), TypeHelper.typeFromString(parts[1].trim()), Direction.valueOf(parts[0]), false));
                }
            }
        } catch (Exception e) {
            throw new ParsingException("Error parsing '"+string+"': "+e.getLocalizedMessage(), e);
        }
        return result;
    }
    
    @Override
    public String toString() {
        try {
            return (dir == Direction.IN ? "" : dir.toString()+" ")+TypeHelper.stringFromType(type)+" "+name;
        } catch (TypeHelper.UnknownTypeException ex) {
            return type+" "+name;
        }
    }
}
