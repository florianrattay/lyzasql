/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.esotericsoftware.yamlbeans.YamlReader;
import de.thomas_oster.lazysql.annotations.LazySQLConfig;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import lombok.EqualsAndHashCode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
@EqualsAndHashCode(of = {"dburl","dbuser","dbpassword","cachefile","customDriver", "dependencyInjection", "addJsonAnnotations"})
public class DbConfig {
    public static class ConfigException extends Exception {
        public ConfigException(Exception ex) {
            super(ex);
            if (LazySQLProcessor.DEBUG_TO_FILE) {
                LazySQLProcessor.debugLog(ex.getLocalizedMessage());
            }
        }
        public ConfigException(String message) {
            super(message);
            if (LazySQLProcessor.DEBUG_TO_FILE) {
                LazySQLProcessor.debugLog(message);
            }
        }
    };
    
    public String dburl;
    public String dbuser;
    public String dbpassword;
    public String cachefile;
    public String customDriver;
    public boolean dependencyInjection = false;
    public boolean addJsonAnnotations = false;
    public String[] excludeColumns;
    
    public static DbConfig fromEnvironment() {
        DbConfig result = new DbConfig();
        result.dburl = System.getenv("LAZYSQL_DB");
        result.dbuser = System.getenv("LAZYSQL_DB_USER");
        result.dbpassword = System.getenv("LAZYSQL_DB_PASSWORD");
        result.cachefile = System.getenv("LAZYSQL_CACHEFILE");
        result.customDriver = System.getenv("LAZYSQL_CUSTOMDATABASEDRIVER");
        result.dependencyInjection = Boolean.parseBoolean(System.getenv("LAZYSQL_DEPENDENCYINJECTION"));
        result.addJsonAnnotations = Boolean.parseBoolean(System.getenv("LAZYSQL_ADDJSONANNOTATIONS"));
        return StringUtils.isEmpty(result.dburl) ? null : result;
    }
    
    static File findResourcesDir() {
        try {
            return new File(DbConfig.class.getClassLoader().getResource(".").getFile());
        }
        catch (NullPointerException e) {
            //Dummy Test für Eclipse
            if (LazySQLProcessor.DEBUG_TO_FILE) {
                LazySQLProcessor.debugLog("Cannot find resources dir. returning dummy...");
                return new File("/home/thomas/eclipse-workspace/UpstartWMS/src/main/resources");
            }
            throw e;
//            TODO: alternativer Ansatz... muss man probieren...
//            try {
//                JavaFileObject generationForPath = processingEnv.getFiler().createSourceFile("lazysqldummy");
//                Writer writer = generationForPath.openWriter();
//                String sourcePath = generationForPath.toUri().getPath();
//                writer.close();
//                generationForPath.delete();
//
//                return sourcePath;
//            } catch (IOException e) {
//                processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "Unable to determine source file path!");
//            }
            
        }
        
    }
    
    static File findFileInDirOrParents(File dir, String name, int maxSteps) {
        File result = new File(dir, name);
        if (result.exists()) {
            return result;
        }
        return maxSteps > 0 ? findFileInDirOrParents(dir.getParentFile(), name, maxSteps-1) : null;
    }
    
    public static DbConfig fromConfigAnnotation(LazySQLConfig cfg) throws ConfigException {
        if (!StringUtils.isEmpty(cfg.file())) {
            return fromConfigFile(cfg.file());
        }
        DbConfig result = new DbConfig();
        result.dburl = cfg.dburl();
        result.dbuser = cfg.user();
        result.dbpassword = cfg.password();
        result.cachefile = cfg.cacheFilePath();
        result.customDriver = cfg.customDatabaseDriver();
        result.dependencyInjection = cfg.dependencyInjection();
        result.addJsonAnnotations = cfg.addJsonAnnotations();
        result.excludeColumns = cfg.excludeColumns();
        return result;
    }
    public static DbConfig fromSpringBootConfig(String filename) throws ConfigException {
        try {
            File cfgf = new File(filename);
            if (!cfgf.exists()) {
                cfgf = findFileInDirOrParents(findResourcesDir(), filename, 3);
            }
            if (cfgf == null) {
                throw new ConfigException("Cant't find file "+filename+". I tried "+findResourcesDir().getAbsolutePath()+" and 3 parents");
            }
            DbConfig result = new DbConfig();
            //when detecting spring boot we default to jackson and injection
            result.addJsonAnnotations = true;
            result.dependencyInjection = true;
            Map<String,Object> cfg = (Map<String,Object>) new YamlReader(FileUtils.readFileToString(cfgf, "UTF8")).read();
            if (cfg.get("spring") instanceof Map && ((Map) cfg.get("spring")).get("datasource") instanceof Map) {
                Map<String,String> ds = (Map<String,String>) ((Map) cfg.get("spring")).get("datasource");
                result.customDriver = ds.getOrDefault("driverClassName", result.customDriver);
                result.dburl = ds.getOrDefault("url", result.dburl);
                result.dbpassword = ds.getOrDefault("password", result.dbpassword);
                result.dbuser = ds.getOrDefault("username", result.dbuser);
            }
            else if (cfg.get("spring.datasource") instanceof Map) {
                Map<String,String> ds = (Map<String,String>) cfg.get("spring.datasource");
                result.customDriver = ds.getOrDefault("driverClassName", result.customDriver);
                result.dburl = ds.getOrDefault("url", result.dburl);
                result.dbpassword = ds.getOrDefault("password", result.dbpassword);
                result.dbuser = ds.getOrDefault("username", result.dbuser);
            }
            if (cfg.get("lazysql") != null) {
                Map<String,String> lcfg = (Map<String,String>) cfg.get("lazysql");
                result.customDriver = lcfg.getOrDefault("customDriver", result.customDriver);
                result.dbpassword = lcfg.getOrDefault("dbpassword", result.dbpassword);
                result.dburl = lcfg.getOrDefault("dburl", result.dburl);
                result.dbuser = lcfg.getOrDefault("dbuser", result.dbuser);
                result.cachefile = lcfg.getOrDefault("cachefile", result.cachefile);
                result.dependencyInjection = Boolean.parseBoolean(lcfg.getOrDefault("dependencyInjection", ""+result.dependencyInjection));
                result.addJsonAnnotations = Boolean.parseBoolean(lcfg.getOrDefault("addJsonAnnotations", ""+result.addJsonAnnotations));
                result.excludeColumns = lcfg.getOrDefault("excludeColumns", "").split(",");
            }
            return StringUtils.isEmpty(result.dburl) ? null : result;
        } catch (IOException ex) {
            throw new ConfigException(ex);
        }
    }
    public static DbConfig fromConfigFile(String filename) throws ConfigException {
        File cfgf = new File(filename);
        if (!cfgf.exists()) {
            cfgf = findFileInDirOrParents(findResourcesDir(), filename, 3);
        }
        if (cfgf == null) {
            throw new ConfigException("Cant't find file "+filename+". I tried "+findResourcesDir().getAbsolutePath()+" and 3 parents");
        }
        return fromConfigFile(cfgf);
    }
    public static DbConfig fromConfigFile(File file) throws ConfigException {
        try {
            CharSequence json = FileUtils.readFileToString(file, "UTF8");
            if (file.getName().toLowerCase().endsWith(".yml")) {
                return new YamlReader(json.toString()).read(DbConfig.class);
            }
            //LazySQLProcessor.debugLog("Picked up config "+file.getAbsolutePath());
            return (DbConfig) (new Gson()).fromJson(json.toString(), DbConfig.class);
        } catch (IOException ex) {
            throw new ConfigException(ex);
        }
    }
    
}
