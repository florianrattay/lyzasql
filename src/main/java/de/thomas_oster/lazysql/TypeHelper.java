/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.squareup.javapoet.TypeName;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
public class TypeHelper {

    /**
     * returns the matching java.sql.types.CONSTANT for the type as String
     * @param type
     * @return 
     */
    static String getSQLType(TypeName type) {
        //TODO: complete
        if (type.equals(TypeName.get(Integer.class))) {
            return "java.sql.Types.INTEGER";
        }
        return "java.sql.Types.VARCHAR";
    }

    public static class UnknownTypeException extends Exception {
        public UnknownTypeException(String message) {
            super(message);
        }
    }
    
    public static String getSimpleName(String s) {
        String[] parts = s.split("\\.");
        return parts[parts.length - 1];
    }

    public static String getSimpleName(TypeName t) {
        return getSimpleName(t.toString());
    }

    private static final BiMap<String, TypeName> typeFromString;

    static {
        Class[] classes = new Class[]{
            String.class,
            Integer.class,
            Double.class,
            byte[].class,
            Date.class,
            Boolean.class,
            Short.class,
            Long.class,
            Float.class,
            Byte.class,
            byte.class,
            int.class,
            double.class,
            short.class,
            boolean.class,
            BigDecimal.class,
            BigInteger.class,
            Timestamp.class
        };
        typeFromString = HashBiMap.create(classes.length);
        for (Class c : classes) {
            typeFromString.put(c.getSimpleName(), TypeName.get(c));
        }
    }

    public static TypeName typeFromString(String simpleName) throws UnknownTypeException {
        TypeName r = typeFromString.get(simpleName);
        if (r == null) {
            throw new UnknownTypeException("Type "+simpleName+" ist unbekannt");
        }
        return r;
    }

    public static String stringFromType(TypeName t) throws UnknownTypeException {
        //gleich wie getSimpleName??
        String r = typeFromString.inverse().get(t);
        if (r == null) {
            throw new UnknownTypeException("No simple name for Type "+t);
        }
        return r;
    }
}
