/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.thomas_oster.lazysql.annotations.LazySQLConfig;
import de.thomas_oster.lazysql.annotations.Returns;
import de.thomas_oster.lazysql.annotations.LazySQLUpsert;
import de.thomas_oster.lazysql.annotations.LazySQLStoredProcedure;
import de.thomas_oster.lazysql.annotations.LazySQLUpdate;
import de.thomas_oster.lazysql.annotations.LazySQLSelect;
import de.thomas_oster.lazysql.annotations.LazySQLInsert;
import com.google.auto.service.AutoService;
import com.google.common.base.Strings;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import de.thomas_oster.lazysql.Argument.MismatchException;
import de.thomas_oster.lazysql.DbConfig.ConfigException;
import de.thomas_oster.lazysql.annotations.LazySQLExec;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Completion;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.*;
import javax.lang.model.util.Elements;
import javax.sql.DataSource;
import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.jilt.Builder;
import org.jilt.BuilderStyle;

/**
 *
 * @author Thomas Oster (mail@thomas-oster.de)
 */
@SupportedAnnotationTypes({
    "de.thomas_oster.lazysql.annotations.LazySQLSelect",
    "de.thomas_oster.lazysql.annotations.LazySQLConfig",
    "de.thomas_oster.lazysql.annotations.LazySQLUpdate",
    "de.thomas_oster.lazysql.annotations.LazySQLExec",
    "de.thomas_oster.lazysql.annotations.LazySQLUpsert",
    "de.thomas_oster.lazysql.annotations.LazySQLInsert",
    "de.thomas_oster.lazysql.annotations.LazySQLStoredProcedure",
    "de.thomas_oster.layzsql.annotations.Returns"
})
@AutoService(Processor.class)
public class LazySQLProcessor extends AbstractProcessor {

    private final Pattern sqlParams = Pattern.compile(":[a-zA-Z_]\\w*");

    private TypeName extractTypeName(String fqn) {
        int i = fqn.lastIndexOf(".");
        var packageName = fqn.substring(0, i);
        var returnType = fqn.substring(i+1);
        return ClassName.get(packageName, returnType);
    }

    //From https://stackoverflow.com/questions/7687829/java-6-annotation-processing-getting-a-class-from-an-annotation/52793839#52793839
    //we cannot access the class types directly.
    @FunctionalInterface
    public interface GetClassValue {
        void execute() throws MirroredTypeException, MirroredTypesException;
    }

    public static List<? extends TypeMirror> getTypeMirrorFromAnnotationValue(GetClassValue c) {
        try {
            c.execute();
        }
        catch(MirroredTypesException ex) {
            return ex.getTypeMirrors();
        }
        return null;
    }

    public static TypeMirror getFirstTypeMirrorFromAnnotationValue(GetClassValue c) {
        try {
            c.execute();
        }
        catch(MirroredTypesException ex) {
            var result = ex.getTypeMirrors();
            if (result != null && !result.isEmpty()) {
                return result.get(0);
            }
        }
        return null;
    }

    private void addMethodForSelect(String packageName, String className, TypeSpec.Builder lazyDbClass, LazySelect q) throws IOException {
        String returnType = q.annotation.returnClassName();
        TypeName returnTypeClass = ClassName.get(packageName, className, returnType);
        TypeName returnTypeInterface = returnTypeClass;
        var additionalInterfacesC = getTypeMirrorFromAnnotationValue(() -> q.annotation.additionalInterfacesC());
        boolean extraClassForReturnType = false;
        if (StringUtils.isEmpty(returnType)) {
            returnType = q.methodName.substring(0, 1).toUpperCase() + q.methodName.substring(1) + "Result";
            returnTypeClass = ClassName.get(packageName, className, returnType);
            returnTypeInterface = returnTypeClass;
            if (!StringUtils.isBlank(q.annotation.returnInterfaceName())) {
                //Use the interface as return type if class name is not explicitely specified.
                returnTypeInterface = ClassName.get(packageName, className, q.annotation.returnInterfaceName());
            }
            else if (additionalInterfacesC != null && additionalInterfacesC.size() == 1) {
                //If there is exactly one interface specified, use this as return type
                returnTypeInterface = ClassName.get(additionalInterfacesC.get(0));
            }
            else if (q.annotation.additionalInterfaces().length == 1) {
                //If there is exactly one interface specified, use this as return type
                var iface = q.annotation.additionalInterfaces()[0];
                returnTypeInterface = iface.contains(".") ? extractTypeName(iface) : ClassName.get(packageName, className, iface);
            }
        }
        if (returnType.contains(".")) {
            extraClassForReturnType = true;
            returnTypeClass = extractTypeName(returnType);
            returnTypeInterface = returnTypeClass;
        }
        MethodSpec.Builder queryMethod = MethodSpec.methodBuilder(q.methodName)
                .addModifiers(Modifier.PUBLIC);
        if (!q.annotation.convertSqlExceptionToRuntimeException()) {
            queryMethod.addException(SQLException.class);
        }
        if (!q.annotation.manageConnectionWithDatasource()) {
            queryMethod.addParameter(Connection.class, "con");
        }
        else if (!q.config.dependencyInjection) {
            queryMethod.addParameter(DataSource.class, "ds");
        }
        if (q.resultType.size() == 1) {
            if (q.annotation.returnFirstOrNull()) {
                queryMethod.returns(q.resultType.iterator().next().getType());
            }
            else {
                queryMethod.returns(ParameterizedTypeName.get(ClassName.get(List.class), q.resultType.iterator().next().getType()));
            }
        }
        else {
            if (q.annotation.doNotGenerateClass()) {
                //do nothing
            }
            else {
                /** Type for the return value of the select query **/
                var returnClass = buildMutableTypeFor(returnType, q.resultType, q.config.addJsonAnnotations);

                if (additionalInterfacesC != null) {
                    for (var iface : additionalInterfacesC) {
                        returnClass.addSuperinterface(ClassName.get(iface));
                    }
                }
                if (q.annotation.additionalInterfaces() != null) {
                    for (var iface : q.annotation.additionalInterfaces()) {
                        var interfaceName = iface.contains(".") ? extractTypeName(iface) : ClassName.get(packageName, className, iface);
                        returnClass.addSuperinterface(interfaceName);
                    }
                }

                if (!StringUtils.isBlank(q.annotation.returnInterfaceName())) {
                    //generate interface with all getters+setters
                    var returnInterfaceBuilder = buildInterfaceTypeFor(
                            q.annotation.returnInterfaceName(),
                            q.resultType);
                    if (q.annotation.additionalInterfaces() != null) {
                        for (var iface : q.annotation.additionalInterfaces()) {
                            var interfaceName = iface.contains(".") ? extractTypeName(iface) : ClassName.get(packageName, className, iface);
                            returnInterfaceBuilder.addSuperinterface(interfaceName);
                        }
                    }
                    if (additionalInterfacesC != null) {
                        for (var iface : additionalInterfacesC) {
                            returnInterfaceBuilder.addSuperinterface(ClassName.get(iface));
                        }
                    }
                    var returnInterface = returnInterfaceBuilder.build();
                    var returnInterfaceName = ClassName.get(packageName, className, q.annotation.returnInterfaceName());
                    //add the intercface to the lazydb class
                    lazyDbClass.addType(returnInterface);
                    returnClass.addSuperinterface(returnInterfaceName);
                }
                if (extraClassForReturnType) {
                    try {
                        JavaFile.builder(packageName, returnClass.build()).build().writeTo(filer);
                    } catch (IOException ex) {
                        messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), q.element);
                    }
                } else {
                    lazyDbClass.addType(
                            returnClass
                                    .addModifiers(Modifier.STATIC)
                                    .build()
                    );
                }
            }
            if (q.annotation.returnFirstOrNull()) {
                queryMethod.returns(returnTypeInterface);
            }
            else {
                queryMethod.returns(ParameterizedTypeName.get(ClassName.get(List.class), returnTypeInterface));
            }
        }
        if (q.annotation.dynamicWhereClause()) {
            queryMethod.addParameter(String.class, "whereClause");
        }
        
        if (q.annotation.useObjectAsInput()) {
            String argumentTypeName = "InputFor" + WordUtils.capitalize(q.methodName);
            TypeSpec argumentType = buildTypeFor(argumentTypeName, q.params, true).build();
            JavaFile.builder(packageName, argumentType).build().writeTo(filer);
            queryMethod.addParameter(ClassName.get(packageName, argumentTypeName), "i");
        }
        else {
            for (Argument a : q.params) {
                queryMethod.addParameter(a.getType(), a.getName());
            }
        }

        List<Argument> argumentsInQueryOrder = new LinkedList<>();
        try
        {
            String query = replaceArgumentsAndListOrder(q.sqlQuery, q.params, argumentsInQueryOrder);
            
            CodeBlock.Builder methodBody = CodeBlock.builder();
            if (!q.annotation.manageConnectionWithDatasource()) {
                methodBody.beginControlFlow("try( \n"
                                            + "\t$T stmtObj = con.prepareStatement(\"" + escapeQuotesAndDollars(query) + " \"" + (q.annotation.dynamicWhereClause() ? ".replace(\"{WHERE}\", whereClause)" : "") + ")\n)", PreparedStatement.class);
            }
            else {
                methodBody.beginControlFlow("try( \n"
                                            + "\t$T con = ds.getConnection();\n"
                                            + "\t$T stmtObj = con.prepareStatement(\"" + escapeQuotesAndDollars(query) + " \"" + (q.annotation.dynamicWhereClause() ? ".replace(\"{WHERE}\", whereClause)" : "") + ")\n)",
                        Connection.class,
                        PreparedStatement.class);
            }
            addCodeForSettingParameters(methodBody, argumentsInQueryOrder, q.annotation.useObjectAsInput());
            if (q.annotation.fetchSize() > 0) {
                methodBody.addStatement("stmtObj.setFetchSize("+q.annotation.fetchSize()+")");
            }
            methodBody.addStatement("$T resObj = stmtObj.executeQuery()", ResultSet.class);
            if (q.resultType.size() == 1) {
                Argument r = q.resultType.iterator().next();
                //TODO if q.annotation.returnFirstOrNull DO NOT ITERATE WHOLE RESULT SET....
                methodBody.addStatement("$L<$T> result = new $L<>()", TypeName.get(List.class), r.getType(), TypeName.get(LinkedList.class))
                    .beginControlFlow("while (resObj.next())");
                if (r.getType().equals(TypeName.get(Integer.class))) {
                    methodBody.addStatement("Integer j = resObj.getInt(\"" + r.name + "\")");
                    methodBody.addStatement("result.add(resObj.wasNull() ? null : j)");
                } else if (r.getType().equals(TypeName.get(byte[].class))) {
                    methodBody.addStatement("result.add(resObj.getBytes(\"" + r.name + "\"))");
                } else {
                    methodBody.addStatement("result.add(resObj.get" + TypeHelper.getSimpleName(r.getType()) + "(\"" + r.name + "\"))");
                }
            }
            else {
                //TODO if q.annotation.returnFirstOrNull DO NOT ITERATE WHOLE RESULT SET....
                methodBody.addStatement("$L<$T> result = new $L<>()", TypeName.get(List.class), returnTypeInterface, TypeName.get(LinkedList.class))
                    .beginControlFlow("while (resObj.next())");
                    methodBody.addStatement("    $T r = new $T()", returnTypeClass, returnTypeClass);
                for (Argument r : q.resultType) {
                    if (r.getType().equals(TypeName.get(Integer.class))) {
                        methodBody.addStatement("r.set" + WordUtils.capitalize(r.name) + "(resObj.getInt(\"" + r.name + "\"))");
                        methodBody.addStatement("if (resObj.wasNull()) { r.set" + WordUtils.capitalize(r.name) + "(null); }");
                    } else if (r.getType().equals(TypeName.get(byte[].class))) {
                        methodBody.addStatement("r.set" + WordUtils.capitalize(r.name) + " (resObj.getBytes(\"" + r.name + "\"))");
                    } else {
                        methodBody.addStatement("r.set" + WordUtils.capitalize(r.name) + "(resObj.get" + TypeHelper.getSimpleName(r.getType()) + "(\"" + r.name + "\"))");
                    }
                }
                methodBody.addStatement("result.add(r)");
            }
            methodBody
                .endControlFlow();
            if (q.annotation.returnFirstOrNull()) {
                methodBody.addStatement("return result.isEmpty() ? null : result.get(0)");
            }
            else {
                methodBody.addStatement("return result");
            }
            methodBody.endControlFlow();//end try-with resources
            generateCatchClause(methodBody, q.annotation.convertSqlExceptionToRuntimeException());
            queryMethod.addCode(methodBody.build());
            lazyDbClass.addMethod(queryMethod.build());
        }
        catch (MismatchException e) {
            messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), q.element);
        }
    }
    
    /**
     * Replaces all :Argument strings in the SQL query with ?,
     * checks if they are present in the excpectedArguments and
     * put them (possible multiple times) into argumentsInQueryOrder
     * @param query
     * @param parameters List with possible arguments
     * @param argumentsInQueryOrder Result List with Arguemtns in query order
     * @return returns the query with all arguments
     */
    public String replaceArgumentsAndListOrder(String query, List<Argument> parameters, List<Argument> argumentsInQueryOrder) throws MismatchException {
        Matcher m = sqlParams.matcher(query);
        while (m.find()) {
            String name = m.group().substring(1);
            Optional<Argument> first = parameters.stream().filter(a -> a.getName().equals(name)).findFirst();
            if (!first.isPresent()) {
                throw new MismatchException("Query Parameter '" + name + "' nicht gefunden");
            }
            argumentsInQueryOrder.add(first.get());
            query = query.replaceFirst(":" + name + "\\b", "?");
            m = sqlParams.matcher(query);
        }
        return query;
    }
    
    private void addMethodForUpdate(String packageName, TypeSpec.Builder lazyDbClass, LazyUpdate q) throws IOException {
        
        MethodSpec.Builder queryMethod = MethodSpec.methodBuilder(q.methodName)
                .addModifiers(Modifier.PUBLIC);
        if (!q.annotation.convertSqlExceptionToRuntimeException()) {
            queryMethod.addException(SQLException.class);
        }
        if (!q.annotation.manageConnectionWithDatasource()) {
            queryMethod.addParameter(Connection.class, "con");
        }
        else if (!q.config.dependencyInjection) {
            queryMethod.addParameter(DataSource.class, "ds");
        }
        queryMethod.returns(int.class);
        if (q.annotation.dynamicWhereClause()) {
            queryMethod.addParameter(String.class, "whereClause");
        }
        
        if (q.annotation.useObjectAsInput()) {
            String argumentTypeName = "InputFor" + WordUtils.capitalize(q.methodName);
            TypeSpec argumentType = buildTypeFor(argumentTypeName, q.params, true).build();
            JavaFile.builder(packageName, argumentType).build().writeTo(filer);
            queryMethod.addParameter(ClassName.get(packageName, argumentTypeName), "i");
        }
        else {
            for (Argument a : q.params) {
                queryMethod.addParameter(a.getType(), a.getName());
            }
        }
        
        List<Argument> argumentsInQueryOrder = new LinkedList<>();
        try {
            String query = replaceArgumentsAndListOrder(q.sqlQuery, q.params, argumentsInQueryOrder);
            CodeBlock.Builder methodBody = CodeBlock.builder();
            if (!q.annotation.manageConnectionWithDatasource()) {
                methodBody.beginControlFlow("try( \n"
                                            + "\t$T stmtObj = con.prepareStatement(\"" + escapeQuotesAndDollars(query) + " \"" + (q.annotation.dynamicWhereClause() ? ".replace(\"{WHERE}\", whereClause)":"") + "))", PreparedStatement.class);
            }
            else {
                methodBody.beginControlFlow("try( \n"
                                            + "\t$T con = ds.getConnection();\n"
                                            + "\t$T stmtObj = con.prepareStatement(\"" + escapeQuotesAndDollars(query) + " \"" + (q.annotation.dynamicWhereClause() ? ".replace(\"{WHERE}\", whereClause)":"") + "))",
                        Connection.class,
                        PreparedStatement.class);
            }

            addCodeForSettingParameters(methodBody, argumentsInQueryOrder, q.annotation.useObjectAsInput());
            methodBody.addStatement("return stmtObj.executeUpdate()");
            methodBody.endControlFlow();//end try-with resources
            generateCatchClause(methodBody, q.annotation.convertSqlExceptionToRuntimeException());
            queryMethod.addCode(methodBody.build());
            lazyDbClass.addMethod(queryMethod.build());
        }
        catch (MismatchException e) {
            messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), q.element);
        }
    }

    private static String escapeQuotesAndDollars(String query) {
        return query
                .replace("\"", "\\\"")
                .replace("$", "$$");
    }

    private void addMethodForExec(String packageName, TypeSpec.Builder lazyDbClass, LazyExec q) throws IOException {
        
        MethodSpec.Builder queryMethod = MethodSpec.methodBuilder(q.methodName)
                .addModifiers(Modifier.PUBLIC);
        if (!q.annotation.convertSqlExceptionToRuntimeException()) {
            queryMethod.addException(SQLException.class);
        }
        queryMethod.returns(boolean.class);
        if (!q.annotation.manageConnectionWithDatasource()) {
            queryMethod.addParameter(Connection.class, "con");
        }
        else if (!q.config.dependencyInjection) {
            queryMethod.addParameter(DataSource.class, "ds");
        }
        if (q.annotation.useObjectAsInput()) {
            String argumentTypeName = "InputFor" + WordUtils.capitalize(q.methodName);
            TypeSpec argumentType = buildTypeFor(argumentTypeName, q.params, true).build();
            JavaFile.builder(packageName, argumentType).build().writeTo(filer);
            queryMethod.addParameter(ClassName.get(packageName, argumentTypeName), "i");
        }
        else {
            for (Argument a : q.params) {
                queryMethod.addParameter(a.getType(), a.getName());
            }
        }
        
        List<Argument> argumentsInQueryOrder = new LinkedList<>();
        try {
            String query = replaceArgumentsAndListOrder(q.sqlQuery, q.params, argumentsInQueryOrder);
            CodeBlock.Builder methodBody = CodeBlock.builder();
            //only the preparedStatement is closed automatically, because we
            //use the same connection everywhere
            if (!q.annotation.manageConnectionWithDatasource()) {
                methodBody.beginControlFlow("try( \n"
                                            + "\t$T stmtObj = con.prepareStatement(\"" + escapeQuotesAndDollars(query) + " \"))", PreparedStatement.class);
            }
            else {
                methodBody.beginControlFlow("try( \n"
                                            + "\t$T con = ds.getConnection();\n"
                                            + "\t$T stmtObj = con.prepareStatement(\"" + escapeQuotesAndDollars(query) + " \"))",
                        Connection.class,
                        PreparedStatement.class);
            }

            addCodeForSettingParameters(methodBody, argumentsInQueryOrder, q.annotation.useObjectAsInput());
            methodBody.addStatement("return stmtObj.execute()");
            methodBody.endControlFlow();//try-with-resources
            generateCatchClause(methodBody, q.annotation.convertSqlExceptionToRuntimeException());
            queryMethod.addCode(methodBody.build());
            lazyDbClass.addMethod(queryMethod.build());
        }
        catch (MismatchException e) {
            messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), q.element);
        }
    }

    private static class ClassToGenerate {
        String packageName = "de.thomas_oster";
        String className = "LazyDatabase";
        DbConfig config = null;
        DbQueryTyper db = null;
        List<LazyProcedure> procedures = new LinkedList<>();
        List<LazyInsert> inserts = new LinkedList<>();
        List<LazyUpsert> upserts = new LinkedList<>();
        List<LazyUpdate> updates = new LinkedList<>();
        List<LazySelect> selects = new LinkedList<>();
        List<LazyExec> execs = new LinkedList<>();
    }
    
    private Messager messager;
    private Filer filer;
    private Elements elementUtils;
    private Map<String,ClassToGenerate> toGenerate = new LinkedHashMap<>();

    private TypeSpec.Builder buildInterfaceTypeFor(String name, List<Argument> arguments) {
        if (LazySQLProcessor.DEBUG_TO_FILE) {
            LazySQLProcessor.debugLog("buildInterfaceTypeFor "+name);
        }
        TypeSpec.Builder argumentType = TypeSpec.interfaceBuilder(name);

        argumentType.addModifiers(Modifier.PUBLIC, Modifier.STATIC);

        //getter+setter
        for (Argument a : arguments) {
            MethodSpec.Builder getter = MethodSpec.methodBuilder("get" + WordUtils.capitalize(a.getName()));
            getter.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);
            getter.returns(a.getType());
            argumentType.addMethod(getter.build());

            MethodSpec.Builder setter = MethodSpec.methodBuilder("set" + WordUtils.capitalize(a.getName()));
            setter.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);
            setter.addParameter(a.getType(), a.getName());
            argumentType.addMethod(setter.build());
        }

        return argumentType;
    }

    private TypeSpec.Builder buildMutableTypeFor(String name, List<Argument> arguments, boolean useJacksonAnnotation) {
        if (LazySQLProcessor.DEBUG_TO_FILE) {
            LazySQLProcessor.debugLog("buildMutableTypeFor "+name);
        }
        TypeSpec.Builder argumentType = TypeSpec.classBuilder(name);

        argumentType.addModifiers(Modifier.PUBLIC);
        for (Argument a : arguments) {
            argumentType.addField(a.getType(), a.getName());
        }

        //getter+setter
        for (Argument a : arguments) {
            MethodSpec.Builder getter = MethodSpec.methodBuilder("get" + WordUtils.capitalize(a.getName()));
            getter.addModifiers(Modifier.PUBLIC);
            getter.returns(a.getType());
            getter.addCode(CodeBlock.of("return this." + a.getName() + ";\n"));
            if (useJacksonAnnotation) {
                getter.addAnnotation(AnnotationSpec.builder(JsonProperty.class)
                        .addMember("value", "\""+a.getName()+"\"")
                .build());
            }
            argumentType.addMethod(getter.build());
            
            MethodSpec.Builder setter = MethodSpec.methodBuilder("set" + WordUtils.capitalize(a.getName()));
            setter.addModifiers(Modifier.PUBLIC);
            setter.addParameter(a.getType(), a.getName());
            setter.addCode(CodeBlock.of("this." + a.getName() + " = "+a.getName() + ";\n"));
            argumentType.addMethod(setter.build());
        }

        return argumentType;
    }
    
    private TypeSpec.Builder buildTypeFor(String name, List<Argument> arguments, boolean createBuilder) {
        TypeSpec.Builder argumentType = TypeSpec.classBuilder(name);

        argumentType.addModifiers(Modifier.PUBLIC, Modifier.FINAL);
        for (Argument a : arguments) {
            argumentType.addField(a.getType(), a.getName(), Modifier.FINAL, Modifier.PUBLIC);
        }
        MethodSpec.Builder aConst = MethodSpec.constructorBuilder();
        if (createBuilder) {
            aConst.addAnnotation(AnnotationSpec.builder(Builder.class)
                    .addMember("style", CodeBlock.of("$T.$L", BuilderStyle.class, BuilderStyle.TYPE_SAFE.name()))
                    .build());
        }
        for (Argument a : arguments) {
            aConst.addParameter(a.getType(), a.getName());
        }
        CodeBlock.Builder bConst = CodeBlock.builder();
        for (Argument a : arguments) {
            bConst.addStatement(("this." + a.getName() + " = " + a.getName()));
        }
        aConst.addCode(bConst.build());

        argumentType.addMethod(aConst.build());
        //getter
        for (Argument a : arguments) {
            MethodSpec.Builder getter = MethodSpec.methodBuilder("get" + WordUtils.capitalize(a.getName().toString()));
            getter.addModifiers(Modifier.PUBLIC);
            getter.returns(a.getType());
            getter.addCode(CodeBlock.of("return this." + a.getName() + ";"));
            argumentType.addMethod(getter.build());
        }

        return argumentType;
    }

    private void addMethodForInsert(String packageName, TypeSpec.Builder classBuilder, LazyInsert p) throws IOException {
        
        MethodSpec.Builder spMethod = MethodSpec.methodBuilder(p.methodName)
                .addModifiers(Modifier.PUBLIC);
        if (!p.annotation.convertSqlExceptionToRuntimeException()) {
            spMethod.addException(SQLException.class);
        }
        if (!p.annotation.manageConnectionWithDatasource()) {
            spMethod.addParameter(Connection.class, "con");
        }
        else if (!p.config.dependencyInjection) {
            spMethod.addParameter(DataSource.class, "ds");
        }
        spMethod.returns(p.annotation.returnGeneratedKey() ? Integer.class : Boolean.class);

        if (p.annotation.useObjectAsInput() && !p.fields.isEmpty()) {
            String argumentTypeName = "InputFor" + WordUtils.capitalize(p.methodName);
            TypeSpec argumentType = buildTypeFor(argumentTypeName, p.fields, true).build();
            JavaFile.builder(packageName, argumentType).build().writeTo(filer);
            spMethod.addParameter(ClassName.get(packageName, argumentTypeName), "i");
        }
        else {
            for (Argument a : p.fields) {
                spMethod.addParameter(a.getType(), a.getName());
            }
        }

        var sql = "INSERT INTO " + p.tableName;
        if (p.fields.isEmpty()) {
            sql += " DEFAULT VALUES";
        }
        else {
            String argsString = StringUtils.repeat("?,",p.fields.size() - 1) + "?";
            String argumentList = p.fields.stream().map(Argument::getName).collect(Collectors.joining(","));
            sql += " (" + argumentList + ") VALUES (" + argsString + ")";
        }
        CodeBlock.Builder methodBody = CodeBlock.builder();
        var preparedStatement = "con.prepareStatement(\""+sql + "\"" + (p.annotation.returnGeneratedKey() ? ", java.sql.Statement.RETURN_GENERATED_KEYS" : "") + ")";
        if (!p.annotation.manageConnectionWithDatasource()) {
            methodBody.beginControlFlow("try( \n"
                                        + "\t$T stmtObj = "+preparedStatement+")", PreparedStatement.class);
        }
        else {
            methodBody.beginControlFlow("try( \n"
                                        + "\t$T con = ds.getConnection();\n"
                                        + "\t$T stmtObj = "+preparedStatement+")",
                    Connection.class,
                    PreparedStatement.class);
        }

        addCodeForSettingParameters(methodBody, p.fields, p.annotation.useObjectAsInput());
        if (p.annotation.returnGeneratedKey()) {
            methodBody.addStatement("stmtObj.execute()");
            methodBody.addStatement("$T rs = stmtObj.getGeneratedKeys()", ResultSet.class);
            methodBody.beginControlFlow("if (rs.next())");
            methodBody.addStatement("return rs.getInt(1)");
            methodBody.endControlFlow();//if
            methodBody.addStatement("throw new SQLException(\"No key generated when inserting into "+p.tableName+"\")");
        }
        else {
            methodBody.addStatement("return stmtObj.execute()");
        }
        methodBody.endControlFlow();//try-with-resources
        generateCatchClause(methodBody, p.annotation.convertSqlExceptionToRuntimeException());
        spMethod.addCode(methodBody.build());
        classBuilder.addMethod(spMethod.build());
    }
    
    private void addMethodForUpsert(String packageName, TypeSpec.Builder classBuilder, LazyUpsert p) throws IOException {
        
        MethodSpec.Builder spMethod = MethodSpec.methodBuilder(p.methodName)
                .addModifiers(Modifier.PUBLIC);
        if (!p.annotation.convertSqlExceptionToRuntimeException()) {
            spMethod.addException(SQLException.class);
        }
        if (!p.annotation.manageConnectionWithDatasource()) {
            spMethod.addParameter(Connection.class, "con");
        }
        else if (!p.config.dependencyInjection) {
            spMethod.addParameter(DataSource.class, "ds");
        }
        spMethod.returns(Boolean.class);

        Set<String> separateParams = Set.of(p.annotation.separateParams());

        var classAsInput = getFirstTypeMirrorFromAnnotationValue(() -> p.annotation.useClassAsInput());

        if (!p.annotation.useObjectAsInput()) {
            for (Argument a : p.fields) {
                spMethod.addParameter(a.getType(), a.getName());
            }
        }
        else {
            if (classAsInput != null && classAsInput.getKind() != TypeKind.VOID) {
                spMethod.addParameter(ClassName.get(classAsInput), "i");
            }
            else if (p.annotation.useObjectAsInput()) {
                String argumentTypeName = "InputFor" + WordUtils.capitalize(p.methodName);
                var fieldsWithoutSeparateParams = p.fields;
                if (!separateParams.isEmpty()) {
                    fieldsWithoutSeparateParams = p.fields.stream().filter(f -> !separateParams.contains(f.name)).toList();
                }
                TypeSpec argumentType = buildTypeFor(argumentTypeName, fieldsWithoutSeparateParams, true).build();
                JavaFile.builder(packageName, argumentType).build().writeTo(filer);
                spMethod.addParameter(ClassName.get(packageName, argumentTypeName), "i");
            }
            for (Argument a : p.fields) {
                if (separateParams.contains(a.name)) {
                    spMethod.addParameter(a.getType(), a.getName());
                }
            }
        }

        String argsString = p.fields.isEmpty() ? "" : "(" + StringUtils.repeat("?,",p.fields.size() - 1) + "?)";
        String argumentList = p.fields.stream().map(Argument::getName).collect(Collectors.joining(","));
        
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(p.tableName)
            .append(" SET ").append(p.toUpdate.stream()
                    .map(it -> it + " = ? ").collect(Collectors.joining(",")))
            .append(" WHERE ").append(p.keys.stream()
                    .map(it -> it +" = ? ").collect(Collectors.joining(" AND "))
            ).append(" IF @@ROWCOUNT=0 ")
        .append("INSERT INTO ").append(p.tableName)
                .append(" (").append(argumentList).append(") ")
                .append("VALUES ").append(argsString);
        
        CodeBlock.Builder methodBody = CodeBlock.builder();
        if (!p.annotation.manageConnectionWithDatasource()) {
            methodBody.beginControlFlow("try(\n"
                                        + "\t$T stmtObj = con.prepareStatement(\"" + sql + "\"))", PreparedStatement.class);
        }
        else {
            methodBody.beginControlFlow("try( \n"
                                        + "\t$T con = ds.getConnection();\n"
                                        + "\t$T stmtObj = con.prepareStatement(\"" + sql + "\"))",
                    Connection.class,
                    PreparedStatement.class);
        }

        List<Argument> params = new LinkedList<>();
        params.addAll(Stream.concat(
                p.fields.stream().filter(a -> p.toUpdate.contains(a.name)), //SET setzt alle nicht keys
                p.fields.stream().filter(a -> p.keys.contains(a.name)) //WHERE enthält nur keys
        ).toList());
        params.addAll(p.fields);//und nochmal alle für INSERT
        addCodeForSettingParameters(methodBody, params, p.annotation.useObjectAsInput(), separateParams);
        methodBody.addStatement("return stmtObj.execute()");
        methodBody.endControlFlow();//try-with-resources
        generateCatchClause(methodBody, p.annotation.convertSqlExceptionToRuntimeException());
        spMethod.addCode(methodBody.build());
        classBuilder.addMethod(spMethod.build());
    }

    private void addCodeForSettingParameters(CodeBlock.Builder methodBody, List<Argument> arguments, boolean useGetters) {
        addCodeForSettingParameters(methodBody, arguments, useGetters, Set.of());
    }

    private void addCodeForSettingParameters(CodeBlock.Builder methodBody, List<Argument> arguments, boolean useGetters, Set<String> excludeGetters) {
        int i = 0;
        for (Argument a : arguments) {
            ++i;
            if (a.dir == Argument.Direction.IN || a.dir == Argument.Direction.INOUT) {
                String value = useGetters && !excludeGetters.contains(a.getName())? "i.get" + WordUtils.capitalize(a.getName()) + "()" : a.getName();
                if ("Date".equals(TypeHelper.getSimpleName(a.getType()))) {
                    methodBody.addStatement("stmtObj.setTimestamp(" + i + ", "+ value+" == null ? null : new $T(" + value + ".getTime()))", Timestamp.class);
                }
                else if ("BigInteger".equals(TypeHelper.getSimpleName(a.getType()))) {
                    methodBody.addStatement("stmtObj.setBigDecimal(" + i + ", " + value+" == null ? null : new $T(" + value + "))", BigDecimal.class);
                }
                else if ("Integer".equals(TypeHelper.getSimpleName(a.getType()))) {
                    methodBody.beginControlFlow("if (" + value + " != null)")
                        .addStatement("stmtObj.setInt(" + i + ", " + value + ")")
                        .nextControlFlow("else")
                        .addStatement("stmtObj.setNull(" + i + ", java.sql.Types.INTEGER)")
                    .endControlFlow();
                }
                else {
                    String setMethod = "set" + StringUtils.capitalize(TypeHelper.getSimpleName(a.getType()));
                    if ("setByte[]".equals(setMethod)) {
                        setMethod = "setBytes";
                    }
                    methodBody.addStatement("stmtObj." + setMethod + "(" + i + ", " + value + ")");
                }
            }
            if (a.dir == Argument.Direction.OUT) {
                methodBody.addStatement("stmtObj.registerOutParameter(" + i + ", " + TypeHelper.getSQLType(a.getType()) + ")");
            }
        }
    }
    
    private void addMethodForStoredProcedure(String packageName, String className, TypeSpec.Builder classBuilder, LazyProcedure p) throws IOException {
        
        List<Argument> inputParams = p.params.stream().filter(x -> x.dir == Argument.Direction.IN || x.dir == Argument.Direction.INOUT).collect(Collectors.toList());
        List<Argument> outputParams = p.params.stream().filter(x -> x.dir == Argument.Direction.OUT || x.dir == Argument.Direction.INOUT).collect(Collectors.toList());
        String returnType = p.methodName.substring(0, 1).toUpperCase() + p.methodName.substring(1) + "Result";
        ClassName returnTypeClass = ClassName.get(packageName, className, returnType);
        
        MethodSpec.Builder spMethod = MethodSpec.methodBuilder(p.methodName)
                .addModifiers(Modifier.PUBLIC);
        if (!p.annotation.convertSqlExceptionToRuntimeException()) {
            spMethod.addException(SQLException.class);
        }
        if (!p.annotation.manageConnectionWithDatasource()) {
            spMethod.addParameter(Connection.class, "con");
        }
        else if (!p.config.dependencyInjection) {
            spMethod.addParameter(DataSource.class, "ds");
        }
        if (outputParams.isEmpty()) {
            spMethod.returns(Boolean.class);
        }
        else if (outputParams.size() == 1) {
            spMethod.returns(outputParams.get(0).getType());
        }
        else {
            classBuilder.addType(
                buildMutableTypeFor(returnType, outputParams, p.config.addJsonAnnotations)
                .addModifiers(Modifier.STATIC)
                .build()
            );
            spMethod.returns(returnTypeClass);
        }
        
        if (p.annotation.useObjectAsInput()) {
            String argumentTypeName = "InputFor" + WordUtils.capitalize(p.methodName);
            TypeSpec argumentType = buildTypeFor(argumentTypeName, inputParams, true).build();
            JavaFile.builder(packageName, argumentType).build().writeTo(filer);
            spMethod.addParameter(ClassName.get(packageName, argumentTypeName), "i");
        }
        else {
            for (Argument a : inputParams) {
                spMethod.addParameter(a.getType(), a.getName());
            }
        }

        String argsString = p.params.isEmpty() ? "" : "(" + StringUtils.repeat("?,", p.params.size() - 1) + "?)";
        CodeBlock.Builder methodBody = CodeBlock.builder();
        if (!p.annotation.manageConnectionWithDatasource()) {
            methodBody.beginControlFlow("try(\n"+
                                         "\t$T stmtObj = con.prepareCall(\"{call " + p.procedureName + argsString + "}\"))", CallableStatement.class);
        }
        else {
            methodBody.beginControlFlow("try( \n"
                                        + "\t$T con = ds.getConnection();\n"
                                        + "\t$T stmtObj = con.prepareCall(\"{call " + p.procedureName + argsString + "}\"))",
                    Connection.class,
                    CallableStatement.class);
        }
        addCodeForSettingParameters(methodBody, p.params, p.annotation.useObjectAsInput());
        if (outputParams.isEmpty()) {
            methodBody.addStatement("return stmtObj.execute()");
        }
        else if (outputParams.size() == 1) {
            methodBody.addStatement("stmtObj.execute()");
            Argument r = outputParams.get(0);
            int idx = p.params.indexOf(r)+1;
            if (r.getType().equals(TypeName.get(Integer.class))) {
                methodBody.addStatement("Integer r = stmtObj.getInt("+idx+")");
                methodBody.addStatement("return stmtObj.wasNull() ? null : r");
            } else if (r.getType().equals(TypeName.get(byte[].class))) {
                methodBody.addStatement("return stmtObj.getBytes("+idx+")");
            } else {
                methodBody.addStatement("return stmtObj.get" + TypeHelper.getSimpleName(r.getType()) + "("+idx+")");
            }
        }
        else {
            methodBody.addStatement("stmtObj.execute()");
            methodBody.addStatement("$T r = new $T()", returnTypeClass, returnTypeClass);
            for (Argument r : outputParams) {
                int idx = p.params.indexOf(r)+1;
                if (r.getType().equals(TypeName.get(Integer.class))) {
                    methodBody.addStatement("r.set" + WordUtils.capitalize(r.name) + "(resObj.getInt("+idx+"))");
                    methodBody.addStatement("if (resObj.wasNull()) { r.set" + WordUtils.capitalize(r.name) + "(null); }");
                } else if (r.getType().equals(TypeName.get(byte[].class))) {
                    methodBody.addStatement("r.set" + WordUtils.capitalize(r.name) + " (resObj.getBytes("+idx+"))");
                } else {
                    methodBody.addStatement("r.set" + WordUtils.capitalize(r.name) + "(resObj.get" + TypeHelper.getSimpleName(r.getType()) + "("+idx+"))");
                }
            }
            methodBody.addStatement("return r");
        }
        methodBody.endControlFlow();//try-with-resources
        generateCatchClause(methodBody, p.annotation.convertSqlExceptionToRuntimeException());
        spMethod.addCode(methodBody.build());
        classBuilder.addMethod(spMethod.build());
    }

    private static void generateCatchClause(CodeBlock.Builder methodBody, boolean addCatchClause) {
        if (addCatchClause) {
            methodBody.beginControlFlow("catch ($T e)", SQLException.class);
            methodBody.addStatement("throw new $T(e)", RuntimeException.class);
            methodBody.endControlFlow();
        }
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        List<Completion> result = new LinkedList<>();
        for (Completion c : super.getCompletions(element, annotation, member, userText)) {
            result.add(c);
        }
        for (Completion c : AutoComplete.getCompletions(this, element, annotation, member, userText)) {
            result.add(c);
        }
        return result;
    }
    
    /**
     * Nimmt entweder die Parameter vom übergebenen String, oder falls
     * der leer ist die der Methode. Allerdings werden von der Methode
     * java.sql.Connection , sowie javax.sql.DataSource Parameter ignoriert
     * @param params
     * @param method
     * @return 
     */
    private List<Argument> getArgumentsFromStringOrMethod(String params[], Element method) throws Argument.ParsingException {
        if (params == null || params.length == 0 || (params.length == 1 && StringUtils.isEmpty(params[0]))) {
            ExecutableElement typeElement = (ExecutableElement) method;
            return Argument.fromVariableElements(
                typeElement.getParameters().stream()
                    .filter(p -> 
                        !"java.sql.Connection".equals(p.asType().toString())
                        && !"javax.sql.DataSource".equals(p.asType().toString())
                    )
                .collect(Collectors.toList()));
        }
        else {
            return Argument.fromTypeListString(String.join(",", params).replace(",,", ","));
        }
    }
    
    private void processInsertAnnotations(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(LazySQLInsert.class)) {

            LazySQLInsert annotation = element.getAnnotation(LazySQLInsert.class);
            
            try {
                assertElementIsMethod(element);
                List<Argument> argumentsFromMethod = getArgumentsFromStringOrMethod(annotation.params(), element);
                ClassToGenerate tg = getClassToGenerate(element, roundEnvironment);
                if (tg.db != null) {
                    List<Argument> paramsFromSQL;
                    try {
                        paramsFromSQL = tg.db.getInsertableTableColumns(annotation.value(), null);
                        //remove optional parameters, which are not present in the method
                        var usedNames = argumentsFromMethod.stream().map(a -> a.name).toList();
                        paramsFromSQL.removeIf(p -> p.optional && usedNames.stream().noneMatch(a -> a.equals(p.name)));
                        try {
                            Argument.compareWithOrder(argumentsFromMethod, paramsFromSQL);
                        } catch (MismatchException ex) {
                            messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
                            //use stuff from DB
                            argumentsFromMethod = paramsFromSQL;
                        }
                    } catch (AbstractRDBMSAdapter.NotSupportedException ex) {
                        messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
                    }
                }
                LazyInsert lq = new LazyInsert();
                lq.methodName = StringUtils.isEmpty(annotation.methodName()) ? element.getSimpleName().toString() : annotation.methodName();
                lq.tableName = annotation.value();
                lq.fields = argumentsFromMethod;
                lq.element = element;
                lq.annotation = annotation;
                lq.config = tg.config;
                tg.inserts.add(lq);
            } catch (SQLException | ElementException ex) {
                messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
            }
        }
    }
    
    private void assertElementIsMethod(Element element) throws ElementException {
        if (element.getKind() != ElementKind.METHOD) {
            throw new ElementException("Annotation can only be applied to Methods");
        }
    }
    
    /**
     * Takes an array of strings from an annotation parameter
     * and splits each one by , and returns a list with trimmed non-empty
     * strings
     * @param params
     * @return 
     */
    private List<String> joinCommaSeparated(String[] params) {
        return Arrays.stream(String.join(",", params).replace(",,",",").split(","))
                .map(String::trim)
                .filter(x -> !Strings.isNullOrEmpty(x))
        .collect(Collectors.toList());
    }
    
    private void processUpsertAnnotations(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(LazySQLUpsert.class)) {

            LazySQLUpsert annotation = element.getAnnotation(LazySQLUpsert.class);
            try {
                assertElementIsMethod(element);
                List<Argument> argumentsFromMethod = getArgumentsFromStringOrMethod(annotation.params(), element);
                ClassToGenerate tg = getClassToGenerate(element, roundEnvironment);
                if (tg.db != null) {
                    try {
                        List<Argument> paramsFromSQL = tg.db.getInsertableTableColumns(annotation.table(), null);
                        try {
                            Argument.compareWithOrder(argumentsFromMethod, paramsFromSQL);
                        } catch (MismatchException ex) {
                            messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
                            //use stuff from DB
                            argumentsFromMethod = paramsFromSQL;
                        }
                    }
                    catch (AbstractRDBMSAdapter.NotSupportedException ex) {
                        messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
                    }
                }
                List<String> keys = joinCommaSeparated(annotation.keys());
                //check if all keys are in params
                if (keys.isEmpty()) {
                    messager.printMessage(Kind.MANDATORY_WARNING, "You must specify the keys", element);
                    continue;
                }
                Set<String> argumentNames = argumentsFromMethod.stream().map(Argument::getName).collect(Collectors.toSet());
                Optional<String> missing = keys.stream().filter(k -> !argumentNames.contains(k)).findAny();
                if (missing.isPresent()) {
                    messager.printMessage(Kind.MANDATORY_WARNING, "Key "+missing.get()+" is not a valid table columnn", element);
                    continue;
                }
                List<String> toUpdate = joinCommaSeparated(annotation.onlyUpdate());
                if (toUpdate.isEmpty()) {
                    toUpdate = argumentsFromMethod.stream()
                    .map(Argument::getName)
                    .filter(it -> !keys.contains(it))
                    .collect(Collectors.toList());
                }
                else {
                    //check if all are valid
                    missing = toUpdate.stream().filter(k -> !argumentNames.contains(k)).findAny();
                    if (missing.isPresent()) {
                        messager.printMessage(Kind.MANDATORY_WARNING, "onlyUpdate: "+missing.get()+" is not a valid table columnn", element);
                        continue;
                    }
                }
                
                LazyUpsert lq = new LazyUpsert();
                lq.methodName = StringUtils.isEmpty(annotation.methodName()) ? element.getSimpleName().toString() : annotation.methodName();
                lq.tableName = annotation.table();
                lq.fields = argumentsFromMethod;
                lq.keys = keys;
                lq.element = element;
                lq.annotation = annotation;
                lq.config = tg.config;
                lq.toUpdate = toUpdate;
                tg.upserts.add(lq);
            } catch (SQLException | ElementException ex) {
                messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
            }
        }
    }

    private static class LazyProcedure {
        String methodName;
        String procedureName;
        List<Argument> params;
        Element element;
        LazySQLStoredProcedure annotation;
        DbConfig config;
    }
    
    private static class LazyInsert {
        String methodName;
        String tableName;
        List<Argument> fields;
        Element element;
        LazySQLInsert annotation;
        DbConfig config;
    }
    
    private static class LazyUpsert {
        String methodName;
        String tableName;
        List<Argument> fields;
        List<String> keys;
        List<String> toUpdate;
        Element element;
        LazySQLUpsert annotation;
        DbConfig config;
    }
    
    private static class LazySelect {
        String methodName;
        String sqlQuery;
        List<Argument> resultType;
        List<Argument> params;
        Element element;
        LazySQLSelect annotation;
        DbConfig config;
    }
    
    private static class LazyExec {
        String methodName;
        String sqlQuery;
        List<Argument> params;
        Element element;
        LazySQLExec annotation;
        DbConfig config;
    }
    
    private static class LazyUpdate {
        String methodName;
        String sqlQuery;
        List<Argument> params;
        Element element;
        LazySQLUpdate annotation;
        DbConfig config;
    }
    
    private void generateDatabaseClass(ClassToGenerate tg, Filer filer) throws IOException {

        if (LazySQLProcessor.DEBUG_TO_FILE) {
            LazySQLProcessor.debugLog("Generating DB Class "+tg.packageName+"."+tg.className);
        }
        TypeSpec.Builder lazyDbClass = TypeSpec.classBuilder(tg.className).addModifiers(Modifier.PUBLIC);

        if (tg.config.dependencyInjection) {
            lazyDbClass.addAnnotation(ClassName.get("org.springframework.stereotype", "Component"));
            //If any methods manageConnectionWithDatasource and dependency injection is enabled,
            //we inject the DataSource
            if (
                tg.selects.stream().anyMatch(s -> s.annotation.manageConnectionWithDatasource())
                || tg.execs.stream().anyMatch(s -> s.annotation.manageConnectionWithDatasource())
                || tg.updates.stream().anyMatch(s -> s.annotation.manageConnectionWithDatasource())
                || tg.inserts.stream().anyMatch(s -> s.annotation.manageConnectionWithDatasource())
                || tg.procedures.stream().anyMatch(s -> s.annotation.manageConnectionWithDatasource())
                || tg.upserts.stream().anyMatch(s -> s.annotation.manageConnectionWithDatasource())
            )
            {
                var dataSourceType = ClassName.get("javax.sql", "DataSource");
                lazyDbClass.addField(dataSourceType, "ds", Modifier.FINAL, Modifier.PRIVATE);
                //We need a Constructor which inject this field
                lazyDbClass.addMethod(MethodSpec.constructorBuilder()
                        .addAnnotation(ClassName.get("jakarta.inject", "Inject"))
                        .addParameter(dataSourceType, "dataSource")
                        .addCode("this.ds = dataSource;\n")
                        .addModifiers(Modifier.PUBLIC)
                .build());
            }
        }

        for (LazyProcedure p : tg.procedures) {
            addMethodForStoredProcedure(tg.packageName, tg.className, lazyDbClass, p);
        }

        for (LazyInsert i : tg.inserts) {
            addMethodForInsert(tg.packageName, lazyDbClass, i);
        }
        
        for (LazyUpsert u : tg.upserts) {
            addMethodForUpsert(tg.packageName, lazyDbClass, u);
        }
        
        for (LazySelect s : tg.selects) {
            addMethodForSelect(tg.packageName, tg.className, lazyDbClass, s);
        }
        
        for (LazyExec e : tg.execs) {
            addMethodForExec(tg.packageName, lazyDbClass, e);
        }
        
        for (LazyUpdate u : tg.updates) {
            addMethodForUpdate(tg.packageName, lazyDbClass, u);
        }

        JavaFile.builder(tg.packageName, lazyDbClass.build()).build().writeTo(filer);
    }

    public static final boolean DEBUG_TO_FILE = false;
    public static void debugLog(String text) {
        try {
            FileUtils.writeStringToFile(new File("/tmp/debug"), text+"\n", "utf8", true);
        } catch (IOException ex) {
            
        }
    }
    
    /**
     * Searches for the config for this method. Either by finding a config
     * annotation at it's class, or by finding a global config annotation
     * or by using environment variables
     * @param method
     * @param roundEnvironment
     * @return 
     */
    DbConfig findConfig(Element method, RoundEnvironment roundEnvironment) {
        try {
            //If we have an annotation, this is most important
            Element clazz = method.getEnclosingElement();
            //try to find annotation on class level
            if (clazz.getKind() == ElementKind.CLASS) {
                LazySQLConfig config = clazz.getAnnotation(LazySQLConfig.class);
                if (config != null)  {
                    return DbConfig.fromConfigAnnotation(config);
                }
            }
            if (roundEnvironment != null) {
                Iterator<? extends Element> i = roundEnvironment.getElementsAnnotatedWith(LazySQLConfig.class).iterator();
                while (i.hasNext()) {
                    Element e = i.next();
                    LazySQLConfig config = e.getAnnotation(LazySQLConfig.class);
                    if (config != null && config.global()) {
                        return DbConfig.fromConfigAnnotation(config);
                    }
                }
            }
            DbConfig cfg = DbConfig.fromEnvironment();
            if (cfg != null) {
                return cfg;
            }
        }
        catch (ConfigException e) {
            messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), method);
        }
        //still not found? mabye a lazysql.json in the project dir?
        try {
            return DbConfig.fromConfigFile("lazysql.json");
        }
        catch (DbConfig.ConfigException e) {
            //don't throw because we just guessed...
        }
        //still not found? mabye a lazysql.yml in the project dir?
        try {
            return DbConfig.fromConfigFile("lazysql.yml");
        }
        catch (DbConfig.ConfigException e) {
            //don't throw because we just guessed...
        }
        //try spring boot
        try {
            return DbConfig.fromSpringBootConfig("application.yml");
        }
        catch (DbConfig.ConfigException e) {
            //don't throw because we just guessed...
        }
        return null;
    }
    
    /**
     * Searches for the config for this method. Either by finding a config
     * annotation at it's class, or by finding a global config annotation
     * or by using environment variables
     * @param method
     * @param roundEnvironment
     * @return 
     */
    DbQueryTyper getDatabaseForElement(Element method, RoundEnvironment roundEnvironment, Messager messager) {
        try {
            DbConfig cfg = findConfig(method, roundEnvironment);
            return new DbQueryTyper(cfg, messager);
        } catch (SQLException | DbConfig.ConfigException ex) {
            messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), method);
        } catch (NullPointerException e) {
            //config does not contain database settings...
            return null;
        }
        
        try {
            return new DbQueryTyper(null, messager);
        } catch (SQLException | DbConfig.ConfigException e) {
            messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), method);
        }
        return null;
    }

    private void processStoredProcedureAnnotations(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(LazySQLStoredProcedure.class)) {
            try {
                assertElementIsMethod(element);

                LazySQLStoredProcedure annotation = element.getAnnotation(LazySQLStoredProcedure.class);

                List<Argument> arguments = getArgumentsFromStringOrMethod(annotation.params(), element);
                ClassToGenerate tg = getClassToGenerate(element, roundEnvironment);
                if (tg.db != null) {
                    try {
                        List<Argument> paramsFromSQL = tg.db.getStoredProcedureParameters(String.join("", annotation.value()));
                        try {
                            Argument.compareWithOrder(arguments, paramsFromSQL);
                        } catch (MismatchException e) {
                            messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), element);
                            arguments = paramsFromSQL;
                        }
                    }
                    catch (AbstractRDBMSAdapter.NotSupportedException ex) {
                        messager.printMessage(Kind.WARNING, ex.getLocalizedMessage(), element);
                    }
                }
                else {
                    if (arguments.isEmpty()) {
                        throw new ElementException("Without a DB connection, we need a params declaration");
                    }
                }
                LazyProcedure lq = new LazyProcedure();
                lq.methodName = StringUtils.isEmpty(annotation.methodName()) ? element.getSimpleName().toString() : annotation.methodName();
                lq.procedureName = annotation.value();
                lq.params = arguments;
                lq.element = element;
                lq.annotation = annotation;
                lq.config = tg.config;
                tg.procedures.add(lq);
            }
            catch (ElementException|SQLException e) {
                messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), element);
            }
        }
    }

    /**
     * Prüft ob alle Parameter in der SQL Query vorkommen und gibt sonst eine
     * Fehlermeldung an dem Element aus
     * @param sql
     * @param arguments
     * @param element 
     */
    public void checkIfAllArgumentsAreUsed(String sql, List<Argument> arguments, Element element) throws ElementException {
        for (Argument e : arguments) {
            String name = e.getName();
            if (!Pattern.compile(":" + name + "\\b").matcher(sql).find()) {
                messager.printMessage(Diagnostic.Kind.WARNING, "Der Methodenparameter " + name + " wird nicht in der SQL Query verwendet", element);
            }
            sql = sql.replaceAll(":" + name+"\\b", "?");
        }
        Matcher m = sqlParams.matcher(sql);
        if (m.find()) {
            throw new ElementException("Die SQL Variable " + m.group() + " existiert nicht als Parameter");
        }
    }
    
    private void processSelectAnnotations(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(LazySQLSelect.class)) {

            try {
                assertElementIsMethod(element);

                LazySQLSelect annotation = element.getAnnotation(LazySQLSelect.class);
                String sql = extractQuery(annotation.value());

                if (LazySQLProcessor.DEBUG_TO_FILE) {
                    LazySQLProcessor.debugLog("Processing SELECT:  "+sql);
                }
                
                if (annotation.dynamicWhereClause() && !sql.contains("{WHERE}")) {
                    throw new ElementException("Your query must contain {WHERE} when using dynamic where clause");
                }
                
                List<Argument> argumentsFromMethod = getArgumentsFromStringOrMethod(annotation.params(), element);
                checkIfAllArgumentsAreUsed(sql, argumentsFromMethod, element);

                Returns ran = element.getAnnotation(Returns.class);
                String rav = ran != null ? ran.value() : String.join(",", annotation.returns()).replace(",,",",");
                List<Argument> type = null;
                ClassToGenerate tg = getClassToGenerate(element, roundEnvironment);
                if (tg.db == null) {
                    if (StringUtils.isEmpty(rav)) {
                        messager.printMessage(Kind.MANDATORY_WARNING, "Cannot determine the result Type. You need a returns value or a database connection.", element);
                        continue;
                    } else {
                        type = Argument.fromTypeListString(rav);
                        if (type == null) {
                            messager.printMessage(Kind.MANDATORY_WARNING, "Cannot determine the result Type. Invalid returns annotation.", element);
                            continue;
                        } else {
                            messager.printMessage(Kind.WARNING, "No DB Connection. The return type is not checked whatsoever", element);
                        }
                    }
                } else {
                    String checksql = sql.replaceAll(":", "@");
                    if (annotation.dynamicWhereClause()) {
                        checksql = checksql.replace("{WHERE}", "WHERE 1 = 1");
                    }
                    try {
                        type = tg.db.getSqlQueryType(checksql, argumentsFromMethod);
                        var excludedColumns = annotation.excludeColumns() != null ? annotation.excludeColumns() : tg.config.excludeColumns;
                        if (excludedColumns != null) {
                            var exclude = new LinkedHashSet<String>(Arrays.stream(excludedColumns).toList());
                            //ignore the listed types from the database
                            type.removeIf(t -> exclude.contains(t.name));
                        }
                        String asText = Argument.toTypeListString(type);
                        if (!StringUtils.isEmpty(rav)) {
                            List<Argument> at = Argument.fromTypeListString(rav);
                            try {
                                Argument.compareWithoutOrder(type, at);
                            }
                            catch (MismatchException e) {
                                messager.printMessage(Kind.MANDATORY_WARNING, "returns information is different from Database: "
                                        +e.getLocalizedMessage()+"\nShould be:\n" + asText, 
                                element);
                            }
                        } else {
                            messager.printMessage(Kind.WARNING, "Should have returns information:\nreturns = \"" + asText+"\"", element);
                        }
                    }
                    catch (AbstractRDBMSAdapter.NotSupportedException ex) {
                        
                        type = Argument.fromTypeListString(rav);
                        if (type == null) {
                            messager.printMessage(Kind.MANDATORY_WARNING, "Cannot determine the result Type. Invalid returns annotation.", element);
                            continue;
                        } else {
                            messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
                            messager.printMessage(Kind.MANDATORY_WARNING, "The return type is not checked whatsoever", element);
                        }
                    }
                }


                LazySelect lq = new LazySelect();
                lq.methodName = StringUtils.isEmpty(annotation.methodName()) ? element.getSimpleName().toString() : annotation.methodName();
                lq.resultType = type;
                lq.sqlQuery = sql;
                lq.params = argumentsFromMethod;
                lq.annotation = annotation;
                lq.element = element;
                lq.config = tg.config;
                tg.selects.add(lq);
            }
            catch (SQLException|ElementException ex) {
                messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), element);
            }
        }
    }
    
    private void processUpdateAnnotations(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(LazySQLUpdate.class)) {
            try {
                assertElementIsMethod(element);
            
                LazySQLUpdate annotation = element.getAnnotation(LazySQLUpdate.class);
                String sql = extractQuery(annotation.value());

                List<Argument> argumentsFromMethod = getArgumentsFromStringOrMethod(annotation.params(), element);
                checkIfAllArgumentsAreUsed(sql, argumentsFromMethod, element);

                ClassToGenerate tg = getClassToGenerate(element, roundEnvironment);
                if (tg.db == null) {
                    messager.printMessage(Kind.WARNING, "Ohne Datenbankverbindung wird die Query nicht weiter geprüft.", element);
                } else {
                    try {
                        tg.db.checkSQLSyntax(sql.replaceAll(":", "@"), argumentsFromMethod);
                    } catch (AbstractRDBMSAdapter.NotSupportedException ex) {
                        messager.printMessage(Kind.WARNING, ex.getLocalizedMessage(), element);
                    }
                }

                LazyUpdate lq = new LazyUpdate();
                lq.methodName = StringUtils.isEmpty(annotation.methodName()) ? element.getSimpleName().toString() : annotation.methodName();
                lq.sqlQuery = sql;
                lq.params = argumentsFromMethod;
                lq.element = element;
                lq.annotation = annotation;
                lq.config = tg.config;
                tg.updates.add(lq);
            }
            catch (SQLException|ElementException e) {
                messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), element);
            }
        }
    }
    
    /**
     * Concats the annotation value to a single query string and
     * supports the file:// special string for loading the sql from
     * a file
     * @param annotationValue
     * @return 
     */
    private String extractQuery(String[] annotationValue) throws ElementException {
        String result = String.join(" ", annotationValue).replace("\n", " ").replace("\r", "");
        if (result.startsWith("file://")) {
            String filename = result.replaceFirst("file://", "");
            File f = DbConfig.findFileInDirOrParents(DbConfig.findResourcesDir(), filename, 3);
            if (f == null || !f.exists()) {
                throw new ElementException("File "+filename+" not found");
            }
            try {
                result = FileUtils.readFileToString(f, "UTF8");
            } catch (IOException ex) {
                throw new ElementException(ex.getLocalizedMessage());
            }
        }
        return result;
    }
    
    private void processExecAnnotations(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(LazySQLExec.class)) {
            try {
            
                assertElementIsMethod(element);
                LazySQLExec annotation = element.getAnnotation(LazySQLExec.class);
                String sql = extractQuery(annotation.value());

                List<Argument> argumentsFromMethod = getArgumentsFromStringOrMethod(annotation.params(), element);
                checkIfAllArgumentsAreUsed(sql, argumentsFromMethod, element);

                ClassToGenerate tg = getClassToGenerate(element, roundEnvironment);
                if (tg.db == null) {
                    messager.printMessage(Kind.WARNING, "Ohne Datenbankverbindung wird die Query nicht weiter geprüft.", element);
                } else {
                    try {
                        tg.db.checkSQLSyntax(sql.replaceAll(":", "@"), argumentsFromMethod);
                    } catch (AbstractRDBMSAdapter.NotSupportedException ex) {
                        messager.printMessage(Kind.WARNING, ex.getLocalizedMessage(), element);
                    }
                }

                LazyExec lq = new LazyExec();
                lq.methodName = StringUtils.isEmpty(annotation.methodName()) ? element.getSimpleName().toString() : annotation.methodName();
                lq.sqlQuery = sql;
                lq.params = argumentsFromMethod;
                lq.element = element;
                lq.annotation = annotation;
                lq.config = tg.config;
                tg.execs.add(lq);
            }
            catch (SQLException|ElementException e) {
                messager.printMessage(Kind.MANDATORY_WARNING, e.getLocalizedMessage(), element);
            }
        }
    }
    
    /**
     * returns the ClassToGenerate for the given method. For each
     * class only one is created with the matching config and then
     * this one is returned for all subsequent calls
     * @param method
     * @param roundEnvironment
     * @return 
     */
    private ClassToGenerate getClassToGenerate(Element method, RoundEnvironment roundEnvironment) {
        if (method.getKind() != ElementKind.METHOD) {
            throw new RuntimeException("Wir können nur Methoden und Klassen annotieren bisher....");
        }
        Element clazz = method.getEnclosingElement();
        String packageName = elementUtils.getPackageOf(clazz).getQualifiedName().toString();
        String className = clazz.getSimpleName().toString();
        String fqcn = packageName+"."+className;
        if (toGenerate.containsKey(fqcn)) {
            return toGenerate.get(fqcn);
        }
        else {
            ClassToGenerate result = new ClassToGenerate();
            result.packageName = packageName;
            result.className = className+"LazyDb";
            result.config = findConfig(method, roundEnvironment);
            if (result.config == null) {
                messager.printMessage(Kind.WARNING, "Could not find any LazySQL Config. Using default values", method);
                result.config = new DbConfig();
            }
            result.db = getDatabaseForElement(method, roundEnvironment, messager);
            toGenerate.put(fqcn, result);
            return result;
        }
    }
    
    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (set.isEmpty()) {
            return false;
        }
        
        processStoredProcedureAnnotations(set, roundEnvironment);
        processInsertAnnotations(set, roundEnvironment);
        processSelectAnnotations(set, roundEnvironment);
        processUpdateAnnotations(set, roundEnvironment);
        processExecAnnotations(set, roundEnvironment);
        processUpsertAnnotations(set, roundEnvironment);

        try {
            for (ClassToGenerate c : toGenerate.values()) {
                generateDatabaseClass(c, filer);
            }
        } catch (IOException ex) {
                Logger.getLogger(LazySQLProcessor.class.getName()).log(Level.SEVERE, null, ex);
            messager.printMessage(Kind.MANDATORY_WARNING, "AA"+ex.getLocalizedMessage());
            for (TypeElement e : set) {
                messager.printMessage(Kind.MANDATORY_WARNING, ex.getLocalizedMessage(), e);
            }
        }
        toGenerate.clear();
        return true;
    }

    @Override
    public synchronized void init(ProcessingEnvironment pe) {
        super.init(pe);
        messager = pe.getMessager();
        if (LazySQLProcessor.DEBUG_TO_FILE) {
            messager = new DebugMessager(messager);
        }
        filer = pe.getFiler();
        elementUtils = pe.getElementUtils();
    }
    
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

}
