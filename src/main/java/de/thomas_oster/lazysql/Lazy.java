/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

/**
 * Class for a Thread-Safe LazyEvaluated supplier which can
 * throw exceptions
 * @author Thomas Oster (thomas.oster@upstart-it.de)
 * @param <T>
 * @param <E>
 */
public class Lazy<T,E extends Throwable> {

    public static interface Supplier<T,E extends Throwable> {
        public T get() throws E;
    }
    
    public static <T,E extends Throwable> Lazy<T,E> of(Supplier<T,E> s) {
        return new Lazy(s);
    }
    
    private volatile boolean initalized = false;
    private volatile T value;
    private final Supplier<T,E> load;

    public Lazy(Supplier<T,E> load) {
        this.load = load;
    }

    /**
     * Calculates the value and caches the result if no Exception
     * occurs. After that only the cached result is returned
     * until the reset method is called (even if the value was null)
     * @return the value calculated, even null
     * @throws E an exception if the supplier method throws one
     */
    public T get() throws E {
        if (!this.initalized) {
            synchronized (this) {
                if (!this.initalized) {
                    value = load.get();
                    this.initalized = true;
                }
            }
        }
        return value;
    }
    
    /**
     * Alias for get() for supporting JavaBean Style
     * @return
     * @throws E 
     */
    public T getValue() throws E {
        return get();
    }

    public synchronized void reset() {
        this.initalized = false;
    }

}
