/*
 * The MIT License
 *
 * Copyright 2019 Thomas Oster <thomas.oster@upstart-it.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.thomas_oster.lazysql;

import de.thomas_oster.lazysql.AbstractRDBMSAdapter.Procedure;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thomas Oster <thomas.oster@upstart-it.de>
 */
public class ArgumentListCacheTest {
    
    @Test
    public void testReadWriteFromToDisk() throws IOException, SQLException, AbstractRDBMSAdapter.NotSupportedException, Argument.ParsingException {
        File testFile = File.createTempFile("lazysqlcache", "unittest");
        ArgumentListCache c = new ArgumentListCache(testFile);
        //write string list
        List<String> result = c.getStrings("somekey", () -> {
            return Arrays.asList("42", "23");
        });
        assertEquals(Arrays.asList("42", "23"), result);
        //test argument list
        List<Argument> result2 = c.get("somekey2", () -> {
            return Argument.fromTypeListString("String test1, Integer test2");
        });
        assertEquals(Argument.fromTypeListString("String test1, Integer test2"), result2);
        //test cached exception
        try {
            c.get("somekey3", () -> {
                throw new SQLException("Should be cached");
            });
            fail("Exception was not thrown...");
        }
        catch (SQLException e) {
            assertEquals("Should be cached", e.getMessage());
        }
        //test procedures
        List<Procedure> result3 = c.getProcedures("somekey4", () -> {
            List<Procedure> r = new LinkedList<>();
            Procedure p = new Procedure();
            p.name = "spTest";
            p.comment = null;
            r.add(p);
            return r;
        });
        assertEquals("spTest", result3.get(0).name);
        assertEquals(null, result3.get(0).comment);
        c.saveToDisk();
        ArgumentListCache c2 = new ArgumentListCache(testFile);
        //everything should be there
        assertEquals(Arrays.asList("42", "23"), c2.getStrings("somekey", null));
        assertEquals(Argument.fromTypeListString("String test1, Integer test2"), c2.get("somekey2", null));
        //test cached exception
        try {
            c2.get("somekey3", null);
            fail("Exception was not thrown...");
        }
        catch (SQLException e) {
            assertEquals("Should be cached", e.getMessage());
        }
        assertEquals("spTest", c2.getProcedures("somekey4", null).get(0).name);
        assertNull(c2.getProcedures("somekey4", null).get(0).comment);
    }
    
    @Test
    public void testIfOnlyEvaluatedOnce() throws Exception {
        ArgumentListCache c = new ArgumentListCache(null);
        //write string list
        List<String> result = c.getStrings("somekey", () -> {
            return Arrays.asList("42", "23");
        });
        assertEquals(Arrays.asList("42", "23"), result);
        //test argument list
        List<Argument> result2 = c.get("somekey2", () -> {
            return Argument.fromTypeListString("String test1, Integer test2");
        });
        assertEquals(Argument.fromTypeListString("String test1, Integer test2"), result2);
        //test cached exception
        try {
            c.get("somekey3", () -> {
                throw new SQLException("Should be cached");
            });
            fail("Exception was not thrown...");
        }
        catch (SQLException e) {
            assertEquals("Should be cached", e.getMessage());
        }
        
        assertEquals(Argument.fromTypeListString("String test1, Integer test2"), result2);
        
        //read again
        assertEquals(Arrays.asList("42", "23"), c.getStrings("somekey", null));
        assertEquals(Argument.fromTypeListString("String test1, Integer test2"), c.get("somekey2", null));
        //test cached exception
        try {
            c.get("somekey3", null);
            fail("Exception was not thrown...");
        }
        catch (SQLException e) {
            assertEquals("Should be cached", e.getMessage());
        }
    }

}
